import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


# HOST =
URL = "https://www.sravni.ru/valjuty/info/btc-rub-1/"
HEADERS = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "user-agent": UserAgent().chrome,
}


def get_html(url, headers, params=None):
    r = requests.get(url=url, headers=headers, params=params)
    return r


def get_content(html: str):
    soup = BeautifulSoup(html, "html.parser")
    divs = soup.find("div", class_='article-content currency')
    cards = {}
    for row in divs.find('p'):
        cards = row
    return cards


def main_for_bitcoin():
    html = get_html(url=URL, headers=HEADERS)
    if html.status_code == 200:
        value = get_content(html=html.text)
    else:
        value = "Error (No connection)"
    return value


def second_main_for_bitcoin():
    html = get_html(url=URL, headers=HEADERS)
    if html.status_code == 200:
        value = get_content(html=html.text)
        value = value.replace(" ", "")
        value = value.replace("1биткоин=", "")
        value = value.split("р")
        value = value[0]
        value = float(value)
    else:
        value = "Error (No connection)"
    return value


if __name__ == '__main__':
    result = second_main_for_bitcoin()
    print(result)
