from gtts import gTTS
import os


def to_speach(*args):
    try:
        os.remove("../speaker/speach.mp3")
    except FileNotFoundError:
        pass
    text_to_speak = ""
    for line in args:
        text_to_speak += line + ". "

    speach = gTTS(text=text_to_speak, lang="ru", slow=False)
    speach.save("../speaker/speach.mp3")
